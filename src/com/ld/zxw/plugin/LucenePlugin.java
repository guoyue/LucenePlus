package com.ld.zxw.plugin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;

import com.alibaba.fastjson.JSONArray;
import com.ld.zxw.add.AddIndex;
import com.ld.zxw.delete.DeleteIndex;
import com.ld.zxw.init.Init;
import com.ld.zxw.query.QueryIndex;
import com.ld.zxw.update.UpdateIndex;

@SuppressWarnings("all")
public class LucenePlugin {
	
	public LucenePlugin(String path,String core,String AnalyzerType) throws IOException{
		//加载
		Init init = new Init(path, core, AnalyzerType);
		init.start();
	}
	
	/**
	 * 添加索引
	 * @param list
	 * @throws IOException
	 */
	public void add(List<HashMap> list) throws IOException{
		AddIndex addIndex = new AddIndex();
		addIndex.Index(list);
	}
	/**
	 * 更新索引
	 * @param list
	 * @throws IOException
	 */
	public void update(List<HashMap> list) throws IOException{
		UpdateIndex updateIndex = new UpdateIndex();
		updateIndex.Index(list);
	}
	
	/**
	 * 查询索引
	 * @param v
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 * @throws InvalidTokenOffsetsException
	 */
	public JSONArray query(String v) throws IOException, ParseException, InvalidTokenOffsetsException{
		QueryIndex index = new QueryIndex();
		return index.Index(v);
	}
	
	
	/**
	 * 清空索引
	 * @throws IOException
	 */
	public void deleteAll() throws IOException{
		DeleteIndex deleteIndex = new DeleteIndex();
		deleteIndex.deleteAllIndex();
	}

}
