package com.ld.zxw.add;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;

import com.ld.zxw.Documents.Documents;
import com.ld.zxw.config.Config;
@SuppressWarnings("all")
public class AddIndex {

	/**
	 * 添加索引
	 * @param list
	 * @throws IOException
	 */
	public void Index(List<HashMap> list) throws IOException{
		IndexWriterConfig conf = new IndexWriterConfig(Config.ANALYZER);
		IndexWriter writer = new IndexWriter(Config.DIRECTORY, conf);
		try {
			for (HashMap hashMap : list) {
				Documents doc = new Documents();
				Iterator iterator = hashMap.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry entry = (Map.Entry) iterator.next();
					doc.put(entry.getKey(), entry.getValue());
				}
				writer.addDocument(doc.getDocument());
			}
			writer.commit();
		} catch (Exception e) {
			throw e;
		}finally {
			writer.close();
		}
	}
}
