package com.ld.zxw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.ld.zxw.Analyzer.AnalyzerType;
import com.ld.zxw.plugin.LucenePlugin;
@SuppressWarnings("all")
public class TestUpdate {
	private static Logger log = Logger.getLogger(TestUpdate.class);

	public static void main(String[] args) throws IOException {
		log.info("更新----start");
		LucenePlugin lucenePlugin = new LucenePlugin("D:/luceneHome/core/", "test", AnalyzerType.IKAnalyzer);
		lucenePlugin.update(getDate());
		log.info("更新----stop");
	}
	
	public static List<HashMap> getDate(){
		List<HashMap> list = new ArrayList<>();
		for (int i = 1; i < 2; i++) {
			HashMap map = new HashMap<>();
			map.put("id", i+"");
			map.put("name", "更新索引-------------");
			map.put("content", "被忽略，因为百度的查询限制在38个汉字以内。");
			map.put("sort", i+"");
			list.add(map);
		}
		return list;
	}
}
