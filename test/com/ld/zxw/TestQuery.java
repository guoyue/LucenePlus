package com.ld.zxw;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.ld.zxw.Analyzer.AnalyzerType;
import com.ld.zxw.plugin.LucenePlugin;
@SuppressWarnings("all")
public class TestQuery {
	private static Logger log = Logger.getLogger(TestQuery.class);

	public static void main(String[] args) throws IOException, ParseException, InvalidTokenOffsetsException {
		log.info("查询----start");
		LucenePlugin lucenePlugin = new LucenePlugin("D:/luceneHome/core/", "test", AnalyzerType.IKAnalyzer);
		JSONArray query = lucenePlugin.query("索引");
		for (Object object : query) {
			System.out.println(JSON.toJSONString(object));
		}
		log.info("查询----stop");
	}
}
