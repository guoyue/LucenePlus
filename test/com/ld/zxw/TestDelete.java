package com.ld.zxw;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.ld.zxw.Analyzer.AnalyzerType;
import com.ld.zxw.plugin.LucenePlugin;
@SuppressWarnings("all")
public class TestDelete {
	
	private static Logger log = Logger.getLogger(TestDelete.class);
	public static void main(String[] args) throws IOException {
		log.info("删除---start");
		LucenePlugin lucenePlugin = new LucenePlugin("D:/luceneHome/core/", "test", AnalyzerType.IKAnalyzer);
		lucenePlugin.deleteAll();
		log.info("删除---stop");
	}
}
